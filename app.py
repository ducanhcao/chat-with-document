import streamlit as st
import openai
import pandas as pd
from langchain_core.prompts import ChatPromptTemplate
from langchain_openai import ChatOpenAI

llm = ChatOpenAI(
    model="gpt-4o",
    temperature=0,
    max_tokens=None,
    timeout=None,
    max_retries=2,
    api_key="sk-proj-VUYpmncAiYMrzSertLhHT3BlbkFJFZiqAhRLgd1Yl6GIvZb1",  # if you prefer to pass api key in directly instaed of using env vars
    # base_url="...",
    # organization="...",
    # other params...
)
prompt = ChatPromptTemplate.from_messages(
    [
        (
            "system",
            "You are an expert in the field of networking. Rely on the information users provide to guide them in troubleshooting.",
        ),
        ("human", "Tên cảnh báo: {alarm_name}. \
        \nThông tin về cảnh báo: {knowledge} \
        \nHãy hướng dẫn cách xử lý cảnh báo theo định dạng: \
        \nMô tả cảnh báo: \
        \nNguyên nhân cảnh báo: \
        \nHướng dẫn xử lý: \
        \n- Bước 1: \
        \n- Bước 2: \
        \n- Bước ..."),
    ]
)

data_file_path = "/home/anhcd/Documents/25_00651-axb25017uen.fb1.xlsx"
df = pd.read_excel(data_file_path, sheet_name="Alarms and Events")
def get_knowledge(user_input):
    row = df[df['Name'] == user_input]
    doc = f'#### Name: \n{row["Name"].values[0]} \n#### Description of Alarm/Event Characteristics: \n{row["Description of Alarm/Event Characteristics"].values[0]} \
        \n#### Alarm/Event Text: \n{row["Alarm/Event Text"].values[0]} \n#### Severity: \n{row["Severity"].values[0]} \n#### Category: \n{row["Category"].values[0]} \
        \n#### Area: \n{row["Area"].values[0]} \n#### Cause for Alarm/Event: \n{row["Cause for Alarm/Event"].values[0]} \
        \n#### Cause for clearing the Alarm: \n{row["Cause for clearing the Alarm"].values[0]} \n#### Consequences: \n{row["Consequences"].values[0]} \
        \n#### Actions to be Taken: \n{row["Actions to be Taken"].values[0]} \n#### Related Alarms/Events: \n{row["Related Alarms/Events"].values[0]} \
        \n#### Access Type: \n{row["Access Type"].values[0]} \n#### Hardware Platform: \n{row["Hardware Platform"].values[0]} \n#### Group: \n{row["Group"].values[0]}'
    knowledge = f'Name of alarm: {row["Name"].values[0]} \n\nDescription of alarm characteristics: {row["Description of Alarm/Event Characteristics"].values[0]} \
        \n\nCause of alarm: {row["Cause for Alarm/Event"].values[0]} \n\nActions to be taken: {row["Actions to be Taken"].values[0]}'

    return doc, knowledge

st.set_page_config(layout="wide")

# Function to get response from OpenAI
def get_openai_response(user_input, knowledge):
    chain = prompt | llm
    ai_response = chain.invoke(
        {
            "alarm_name": user_input,
            "knowledge": knowledge,
        }
    )

    return ai_response.content

# Set up the columns
doc_col, chat_col = st.columns(2)

# Initialize session states
if 'bot_response' not in st.session_state:
    st.session_state['bot_response'] = ""
if 'document_content' not in st.session_state:
    st.session_state['document_content'] = ""

# Chat column with user input and AI bot response
with chat_col:
    st.header("AI supports troubleshooting")
    user_input = st.text_input("Enter your alarm:", "")
    if st.button("Send") or user_input:
        if user_input:
            doc, knowledge = get_knowledge(user_input)
            st.session_state['bot_response'] = get_openai_response(user_input, knowledge)
            st.session_state['document_content'] = doc
            user_input = ""

    st.markdown(f"**AI Bot:**\n{st.session_state['bot_response']}", unsafe_allow_html=True)

# Document column to display the document content
with doc_col:
    st.header("Tài liệu hãng cung cấp")
    st.markdown(st.session_state['document_content'], unsafe_allow_html=True)
