from fastapi import FastAPI, Form, File, UploadFile
# from pydantic import BaseModel
from typing import Optional
from PyPDF2 import PdfReader
import io
import json
from langchain_core.prompts import PromptTemplate
from langchain_core.pydantic_v1 import BaseModel, Field
from langchain_experimental.llms.ollama_functions import OllamaFunctions
import pandas as pd
from langchain_community.llms import Ollama
from langchain_core.prompts import ChatPromptTemplate

llm_name = "phi3:14b"

# Extract ===========================================================
class extract_alarm_info(BaseModel):
    """Extract information about name, description, cause, action from detailed documents of an alarm."""

    name: str = Field(default="NA", description="Name of alarm")
    description: str = Field(default="NA", description="Description of alarm characteristics")
    cause: str = Field(default="NA", description="Cause of alarm")
    action: str = Field(default="NA", description="Actions to be taken")


llm = OllamaFunctions(
    model=llm_name,
    format="json",
)  
prompt = PromptTemplate.from_template(
"""
Alarm name: {alarm_name}
Alarm document: 
{alarm_doc}

Human: {question}
AI: """
)
structured_llm = llm.with_structured_output(extract_alarm_info)
chain = prompt | structured_llm

# Support ===========================================================
sp_data_file_path = "25_00651-axb25017uen.fb1.xlsx"
sp_df = pd.read_excel(sp_data_file_path, sheet_name="Alarms and Events")
def get_knowledge(user_input):
    row = sp_df[sp_df['Name'] == user_input]
    doc = f'#### Name: \n{row["Name"].values[0]} \n#### Description of Alarm/Event Characteristics: \n{row["Description of Alarm/Event Characteristics"].values[0]} \
        \n#### Alarm/Event Text: \n{row["Alarm/Event Text"].values[0]} \n#### Severity: \n{row["Severity"].values[0]} \n#### Category: \n{row["Category"].values[0]} \
        \n#### Area: \n{row["Area"].values[0]} \n#### Cause for Alarm/Event: \n{row["Cause for Alarm/Event"].values[0]} \
        \n#### Cause for clearing the Alarm: \n{row["Cause for clearing the Alarm"].values[0]} \n#### Consequences: \n{row["Consequences"].values[0]} \
        \n#### Actions to be Taken: \n{row["Actions to be Taken"].values[0]} \n#### Related Alarms/Events: \n{row["Related Alarms/Events"].values[0]} \
        \n#### Access Type: \n{row["Access Type"].values[0]} \n#### Hardware Platform: \n{row["Hardware Platform"].values[0]} \n#### Group: \n{row["Group"].values[0]}'
    knowledge = f'Name of alarm: {row["Name"].values[0]} \n\nDescription of alarm characteristics: {row["Description of Alarm/Event Characteristics"].values[0]} \
        \n\nCause of alarm: {row["Cause for Alarm/Event"].values[0]} \n\nActions to be taken: {row["Actions to be Taken"].values[0]}'

    return doc, knowledge

sp_llm = Ollama(
    model=llm_name
)  # assuming you have Ollama installed and have llama3 model pulled with `ollama pull llama3 `

sp_prompt = ChatPromptTemplate.from_messages(
    [
        (
            "system",
            "You are an expert in the field of networking. Rely on the information users provide to guide them in troubleshooting.",
        ),
        ("human", "Tên cảnh báo: {alarm_name} \
        \nThông tin về cảnh báo: {knowledge} \
        \nHãy hướng dẫn cách xử lý cảnh báo theo định dạng: \
        \nMô tả cảnh báo: \
        \nNguyên nhân cảnh báo: \
        \nHướng dẫn xử lý: \
        \n- Bước 1: \
        \n- Bước 2: \
        \n- Bước ..."),
    ]
)

sp_chain = sp_prompt | sp_llm

# App ===============================================================
app = FastAPI()

# class JsonResponse(BaseModel):
#     info: json

def extract_text_from_pdf(file_bytes: bytes) -> str:
    # Create a PdfFileReader object from the bytes
    bytes_io = io.BytesIO(file_bytes)
    pdf_reader = PdfReader(bytes_io)
    text = ""
    # Iterate through each page
    for page in pdf_reader.pages:
        text += page.extract_text()

    return text

@app.post("/extract-info")
async def process_paragraph(alarm: str = Form(...), file: Optional[UploadFile] = File(None)):
    # Read the file content
    file_content = await file.read()

    # Extract text from PDF
    pdf_text = extract_text_from_pdf(file_content)

    # Process AI
    res = chain.invoke(input={"alarm_name":alarm, "alarm_doc":pdf_text, "question":"Extract information about that alarm."})

    return json.loads(res.json())

@app.post("/support")
async def process_paragraph(alarm: str = Form(...)):
    # Read the file content
    _, knowledge = get_knowledge(alarm)
    ai_response = sp_chain.invoke(
        {
            "alarm_name": alarm,
            "knowledge": knowledge,
        }
    )

    return ai_response

if __name__ == "__main__":
    import uvicorn
    uvicorn.run(app, host="0.0.0.0", port=8002)
