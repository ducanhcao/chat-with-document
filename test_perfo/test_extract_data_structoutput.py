import PyPDF2
from langchain_core.prompts import PromptTemplate
from langchain_core.pydantic_v1 import BaseModel, Field
from langchain_experimental.llms.ollama_functions import OllamaFunctions
import json

# Note that the docstrings here are crucial, as they will be passed along
# to the model along with the class name.
class extract_alarm_info(BaseModel):
    """Extract information about name, description, cause, action from detailed documents of an alarm."""

    name: str = Field(default="NA", description="Name of alarm")
    description: str = Field(default="NA", description="Description of alarm characteristics")
    cause: str = Field(default="NA", description="Cause of alarm")
    action: str = Field(default="NA", description="Actions to be taken")


llm = OllamaFunctions(
    model="phi3",
    format="json",
)  # assuming you have Ollama installed and have llama3 model pulled with `ollama pull llama3 `


alarm_name = "SCTP ON CP PREFERRED PRIMARY PATH FAILURE"
file_path = "/home/anhcd/Downloads/Telegram Desktop/vMSC-S High Capacity 18.4 - [ALEXwin].pdf"

def extract_text_pdf2(pdf_path):
    """Extracts text from a PDF file using PyPDF2."""
    with open(pdf_path, 'rb') as pdf_file:
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        text = ""
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()
            text += "\n"
    return text

alarm_doc = extract_text_pdf2(file_path)

prompt = PromptTemplate.from_template(
"""{alarm_doc}

Human: {question}
AI: """
)

structured_llm = llm.with_structured_output(extract_alarm_info)
chain = prompt | structured_llm

res = chain.invoke(input={"alarm_doc":alarm_doc, "question":"Extract information about that alarm."})

print(json.loads(res.json()))

