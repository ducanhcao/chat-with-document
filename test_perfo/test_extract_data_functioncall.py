from langchain_core.pydantic_v1 import BaseModel, Field
from langchain_experimental.llms.ollama_functions import OllamaFunctions
import PyPDF2

# Note that the docstrings here are crucial, as they will be passed along
# to the model along with the class name.
class extract_alarm_info(BaseModel):
    """Extract information about name, description, cause, action from detailed documents of an alarm."""

    name: str = Field(default="NA", description="Name of alarm")
    description: str = Field(default="NA", description="Description of alarm characteristics")
    cause: str = Field(default="NA", description="Cause of alarm")
    action: str = Field(default="NA", description="Actions to be taken")

def extract_text_pdf2(pdf_path):
    """Extracts text from a PDF file using PyPDF2."""
    with open(pdf_path, 'rb') as pdf_file:
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        text = ""
        for page_num in range(len(pdf_reader.pages)):
            page = pdf_reader.pages[page_num]
            text += page.extract_text()
            text += "\n"
    return text

if __name__ == "__main__":

    alarm_name = "SCTP ON CP PREFERRED PRIMARY PATH FAILURE"
    file_path = "/home/anhcd/Downloads/Telegram Desktop/vMSC-S High Capacity 18.4 - [ALEXwin].pdf"

    tools = [extract_alarm_info]

    llm = OllamaFunctions(
        model="phi3",
        format="json",
    )  # assuming you have Ollama installed and have model pulled with `ollama pull model_name `

    llm_with_tools = llm.bind_tools(tools, tool_choice="extract_alarm_info")

    alarm_doc = extract_text_pdf2(file_path)
    # print(alarm_doc)

    query = f"Alarm name: {alarm_name}. \nDetailed documentation about alarms: {alarm_doc} \nPlease extract information about name, description, cause, action from detailed documents of that alarm."

    llm_response = llm_with_tools.invoke(query)
    json_data = llm_response.tool_calls[0]['args']

    new_data = {
        'alarm_name': [json_data['name']],
        'alarm_description': [json_data['description']],
        'cause_for_alarm': [json_data['cause']],
        'action_to_be_taken': [json_data['action']]
    }

    print(new_data)
