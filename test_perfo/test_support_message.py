import pandas as pd
from langchain_core.prompts import ChatPromptTemplate
from langchain_community.llms import Ollama

data_file_path = "/home/anhcd/Documents/25_00651-axb25017uen.fb1.xlsx"

df = pd.read_excel(data_file_path, sheet_name="Alarms and Events")

def get_knowledge(user_input):
    row = df[df['Name'] == user_input]
    doc = f'#### Name: \n{row["Name"].values[0]} \n#### Description of Alarm/Event Characteristics: \n{row["Description of Alarm/Event Characteristics"].values[0]} \
        \n#### Alarm/Event Text: \n{row["Alarm/Event Text"].values[0]} \n#### Severity: \n{row["Severity"].values[0]} \n#### Category: \n{row["Category"].values[0]} \
        \n#### Area: \n{row["Area"].values[0]} \n#### Cause for Alarm/Event: \n{row["Cause for Alarm/Event"].values[0]} \
        \n#### Cause for clearing the Alarm: \n{row["Cause for clearing the Alarm"].values[0]} \n#### Consequences: \n{row["Consequences"].values[0]} \
        \n#### Actions to be Taken: \n{row["Actions to be Taken"].values[0]} \n#### Related Alarms/Events: \n{row["Related Alarms/Events"].values[0]} \
        \n#### Access Type: \n{row["Access Type"].values[0]} \n#### Hardware Platform: \n{row["Hardware Platform"].values[0]} \n#### Group: \n{row["Group"].values[0]}'
    knowledge = f'Name of alarm: {row["Name"].values[0]} \n\nDescription of alarm characteristics: {row["Description of Alarm/Event Characteristics"].values[0]} \
        \n\nCause of alarm: {row["Cause for Alarm/Event"].values[0]} \n\nActions to be taken: {row["Actions to be Taken"].values[0]}'

    return doc, knowledge

user_input = "admAttachCapacityPerRaReached"
doc, knowledge = get_knowledge(user_input)


llm = Ollama(
    model="phi3"
)  # assuming you have Ollama installed and have llama3 model pulled with `ollama pull llama3 `

prompt = ChatPromptTemplate.from_messages(
    [
        (
            "system",
            "You are an expert in the field of networking. Rely on the information users provide to guide them in troubleshooting.",
        ),
        ("human", "Tên cảnh báo: {alarm_name} \
        \nThông tin về cảnh báo: {knowledge} \
        \nHãy hướng dẫn cách xử lý cảnh báo theo định dạng: \
        \nMô tả cảnh báo: \
        \nNguyên nhân cảnh báo: \
        \nHướng dẫn xử lý: \
        \n- Bước 1: \
        \n- Bước 2: \
        \n- Bước ..."),
    ]
)

chain = prompt | llm
ai_response = chain.invoke(
    {
        "alarm_name": user_input,
        "knowledge": knowledge,
    }
)

print(ai_response)